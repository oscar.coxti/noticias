import { Injectable, Query } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RespuestaTopHeadline } from '../interfaces/interfaces';
import { environment } from 'src/environments/environment';


const apiKey = environment.apikey;
const apiUlr = environment.apiUlr;

const headers= new HttpHeaders({ 
  'X-Api-key' : apiKey
});

@Injectable({
  providedIn: 'root'
})
export class NoticiasService {
  headlinesPage = 0;
  categoriaActual= '';
  categoriaPage = 0;
  constructor(private http: HttpClient) {}

  private ejecutarQuery<T>( query: string ) {
     query = apiUlr + query; 
     return this.http.get<T>( query, { headers });
  }

  getTopHeadlines(){
    this.headlinesPage++;
    return this.ejecutarQuery<RespuestaTopHeadline>(`/top-headlines?country=us&page=${ this.headlinesPage}`);
    // return this.http.get<RespuestaTopHeadline>(`http://newsapi.org/v2/everything?domains=wsj.com&apiKey=686ab47315074fba8098e9502f8af653`);
  }

  getTopHeadlinesCategoria( categoria: string) {

    if( this.categoriaActual === categoria ){
      this.categoriaPage++;
    } else { 
      this.categoriaPage = 1;
      this.categoriaActual = categoria;
    }
    return this.ejecutarQuery<RespuestaTopHeadline>(`/top-headlines?country=us&category=${ categoria }&page=${ this.categoriaPage}`);
  }
}
